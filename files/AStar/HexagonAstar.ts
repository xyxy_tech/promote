import AStar from "./AStar/AStar";
import HexagonGrid from "./HexagonGrid";

export default class HexagonAstar {

    _gridSize: cc.Size;

    //二维坐标和六边形坐标映射
    _phyMapData: HexagonGrid[][] = [[]];
    //格子六边形左边索引 方便查找
    _mapData: { [index: string]: HexagonGrid } = {};

    /**
     * 
     * @param w 地图宽度
     * @param h 地图高度
     * @param gridSize  格子大小
     */
    initMap(w, h, gridSize) {
        cc.log('initMap', w, h, gridSize)
        this._gridSize = gridSize;
        let xOffset = 0;
        for (let y = 0; y < h; y++) {
            if (y > 0 && y % 2 == 0) {
                xOffset--;
            }
            for (let x = xOffset; x < w + xOffset; x++) {
                let walkable = false

                let z = -(x + y);
                let grid = new HexagonGrid([x, y, z], walkable, this._mapData);

                if (!this._phyMapData[y]) {
                    this._phyMapData[y] = [];
                }
                this._mapData[`${x}_${y}_${z}`] = grid
                let corx = this._phyMapData[y].push(grid) - 1;
                grid.coor2d = cc.v2(corx, y);
            

            }
        }
    }

    setWalkable(grid: HexagonGrid, walkable) {

    }

    getGrid(coor2d){
        let x = coor2d[0];
        let y = coor2d[1];
        if (!this._phyMapData[y]) {
            return null;
        }

        return this._phyMapData[y][x];
    }

    findPath(from: HexagonGrid, to: HexagonGrid) {
        return AStar.findPath(from, to);
    }


    //坐标转位置
    //当前设定 initMap传入的gridSize 宽高为格子图片分辨率，也就是宽度是六边形的肩宽 高度是六边形边长的2倍 
    coorToLoc(coor: number[]): cc.Vec2 {
        let x = coor[0] * this._gridSize.width + coor[1] * this._gridSize.width / 2; // 每高一层 x要再偏移0.5肩宽
        let y = coor[1] * this._gridSize.height * (3 / 4); // y间隔 3/4格子高度
        return cc.v2(x, y)
    }

    coorToLoc2D(coor2d: number[]): cc.Vec2 {
        let x = coor2d[0] * this._gridSize.width + (coor2d[1]%2 == 1 ? this._gridSize.width / 2 : 0); // 奇数层x要再偏移0.5肩宽
        let y = coor2d[1] * this._gridSize.height * (3 / 4); // y间隔 3/4格子高度
        return cc.v2(x, y)
    }
    //位置转坐标
    locToCoor(loc: cc.Vec2) {

    }

    dump() {
        console.log(this)
    }

}
