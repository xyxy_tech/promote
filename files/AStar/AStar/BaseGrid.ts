/**
 * 配合A*使用的格子基类 各种变体只需继承此类并实现基础方法即可
 * 
 */
export default class BaseGrid {
    //总消耗
    private _f: number;
    public get f(): number {
        return this._f;
    }
    //步数
    public g: number;
    //当前grid到终点步数
    public h: number;
    //坐标 需根据实际情况确定
    coor: number[];
    //是否可通过
    walkable: boolean;
    //邻居节点
    neighbors: BaseGrid[];
    //地图数据
    mapData: any;

    constructor(coor: number[], walkable: boolean = true, mapData: any) {
        this.coor = coor;
        this.walkable = walkable;
        this.mapData = mapData;
        this.neighbors = [];
    }
    //查找可行走的邻居节点
    findNeighbors() {
        throw "请复写该方法，实现查找邻居节点"
    }
    //计算两格子距离
    getCostWithGrid(target: BaseGrid): number {
        throw "请复写该方法，实现计算消耗"
    }
    //使用坐标获取格子
    getGridWithCoor(coor: number[]): BaseGrid {
        throw "请复写该方法，实现通过坐标获取grid"
    }
    //重置
    reset() {
        // this.neighbors = [];//邻居不用滞空
        this.g = this.h = 0;
    }


}