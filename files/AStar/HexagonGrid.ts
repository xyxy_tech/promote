import BaseGrid from "./AStar/BaseGrid";

//六边形格子
//宽高比大概是1.732/2 = 0.866
export default class HexagonGrid extends BaseGrid {

    coor2d:cc.Vec2;
    data:any;

    //mapData全部初始化完成后可用
    public findNeighbors() {
        //六角形坐标守恒定律！ x+y+z = 0
        //top-left
        let tlNb = this.getGridWithCoor([this.coor[0] - 1, this.coor[1] + 1, this.coor[2]]);
        if (tlNb) {
            this.neighbors.push(tlNb);
        }
        //top-right
        let trNb = this.getGridWithCoor([this.coor[0], this.coor[1] + 1, this.coor[2] - 1]);
        if (trNb) {
            this.neighbors.push(trNb);
        }
        //left
        let lNb = this.getGridWithCoor([this.coor[0] - 1, this.coor[1], this.coor[2] + 1]);
        if (lNb) {
            this.neighbors.push(lNb);
        }
        //right
        let rNb = this.getGridWithCoor([this.coor[0] + 1, this.coor[1], this.coor[2] - 1]);
        if (rNb) {
            this.neighbors.push(rNb);
        }
        //bottom-left
        let blNb = this.getGridWithCoor([this.coor[0], this.coor[1] - 1, this.coor[2] + 1]);
        if (blNb) {
            this.neighbors.push(blNb);
        }
        //bottom-right
        let brNb = this.getGridWithCoor([this.coor[0] + 1, this.coor[1] - 1, this.coor[2]]);
        if (brNb) {
            this.neighbors.push(brNb);
        }
        return this.neighbors;
    }

    public getCostWithGrid(targetGrid: HexagonGrid) {
        return Math.max(Math.abs(this.coor[0] - targetGrid.coor[0]), Math.abs(this.coor[1] - targetGrid.coor[1]), Math.abs(this.coor[2] - targetGrid.coor[2]))
    }

    public getGridWithCoor(coor: number[]): HexagonGrid {
        return this.mapData[this.generateIdWithCoor(coor)]
    }

    public generateIdWithCoor(coor) {
        return `${coor[0]}_${coor[1]}_${coor[2]}`;
    };

    public generateId() {
        return this.generateIdWithCoor(this.coor);
    }


}
