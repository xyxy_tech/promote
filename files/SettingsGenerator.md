```javascript
    function generate() {

      if(!fs.existsSync(path.join(PROJ, "preview-templates",))){
        Editor.Dialog.messageBox({message:"插件未初始化，设置预览模板"});
        return;
      }

      Editor.log("开始构建settings");

      const UUIDS = require(`${PROJ}/library/uuid-to-mtime.json`);
      const PROJ_CONF = require(`${PROJ}/settings/project.json`);
      const TEMP = require("./settings-template")

      Editor.log(PROJ_CONF)

      TEMP.collisionMatrix = PROJ_CONF["collision-matrix"];
      TEMP.groupList = PROJ_CONF["group-list"];

      Editor.log(UUIDS[PROJ_CONF["start-scene"]])
      TEMP.launchScene = "db://assets/" + UUIDS[PROJ_CONF["start-scene"]].relativePath.replace(/\\/g,"/")
      Editor.log(TEMP)

      const SCENE_LIST = [];

      for (let uuid in UUIDS) {
        let relativePath = UUIDS[uuid].relativePath;
        let _rPath = path.relative(path.join(PROJ, 'assets', 'resources'), path.join(PROJ, 'assets', relativePath));
        let metaDir = path.join(PROJ, 'assets', relativePath) + '.meta'
        let ext = path.extname(relativePath).toLowerCase();
        let assetData = [];
        if (ext == ".fire") {
            SCENE_LIST.push({
                url: `db://assets/` + relativePath.replace(/\\/g,"/"),
                uuid: uuid
            })
        }
        assetData[0] = _rPath.replace(/\\/g,"/")

        if (ext == ".plist") {
            if (fs.existsSync(metaDir)) {
                let meta = JSON.parse(fs.readFileSync(metaDir));
                if (meta.type) {
                    assetData[1] = "cc.SpriteAtlas";
                } else {
                    assetData[1] = "cc.ParticleAsset";
                }
            }


        } else if (ext == ".json") {
            let data = JSON.parse(fs.readFileSync(path.join(PROJ, 'assets', relativePath)));
            if (data.skeleton && data.skeleton.spine) {
                assetData[1] = "sp.SkeletonData"
            } else {
                assetData[1] = "cc.JsonAsset"
            }
        } else {
            assetData[1] = EXT_TYPE_MAP[ext];
        }
        if (assetData[1])
            TEMP.rawAssets.assets[uuid] = assetData;

        //spriteFrame
        if (assetData[1] == "cc.Texture2D" && fs.existsSync(metaDir)) {
            let meta = JSON.parse(fs.readFileSync(metaDir));
            if (meta.subMetas) {
                let sfInfo = meta.subMetas[path.basename(relativePath, ext)]
                if (sfInfo) {
                    TEMP.rawAssets.assets[sfInfo.uuid] = [
                        assetData[0], "cc.SpriteFrame", 1
                    ]
                }

            }

        }
      }
      TEMP.scenes = SCENE_LIST;
      Editor.log("start write settings");
  
      fs.writeFileSync(PROJ + '/preview-templates/custom-settings.js', 'window._CCSettings = ' + JSON.stringify(TEMP), 'utf-8');
      Editor.log("generate done！")
      Editor.Dialog.messageBox({title:"生成完成",message:"settings 生成完成，你现在可以点击预览了"})
    }

```

```javascript
module.exports = {
    ".prefab": "cc.Prefab",
    ".effect": "cc.EffectAsset",
    ".mtl": "cc.Material",
    ".json": "cc.JsonAsset",
    ".anim": "cc.AnimationClip",
    ".atlas": "cc.Asset",
    ".png": "cc.Texture2D",
    ".jpg": "cc.Texture2D",
    ".jpeg": "cc.Texture2D",
    ".plist": "cc.SpriteAtlas|cc.ParticleAsset",
    ".labelatlas": "cc.LabelAtlas",
    ".fnt": "cc.BitmapFont",
    ".ttf": "cc.TTFFont",
    ".txt":"cc.TextAsset",
    ".pac": "cc.SpriteAtlas",
    ".mp3": "cc.AudioClip",
    ".wav": "cc.AudioClip",
    ".zip": "cc.Asset",
    ".mp4": "cc.Asset",
    // ".fire": "cc.Scene",
    ".tsx": "cc.TextAsset",
    ".tmx": "cc.TiledMapAsset",
    ".effect": "cc.EffectAsset",
}
```