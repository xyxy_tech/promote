```shell
clothes_spine_pre_path="assets/resources/gl_zh/prefabs/spine/clothes"
audio_path="assets/resources/gl_zh/res/audio/cv1"
clothe_icon_path="assets/resources/gl_zh/res/clothe"
clothe_spine_path="assets/resources/gl_zh/res/spine/clothes"
baremold="assets/resources/gl_zh/res/spine/clothes/baremold"
black_baremold="assets/resources/gl_zh/res/spine/clothes/black_baremold"

echo "open git sparse-checkout"
git config core.sparsecheckout true
echo "done"

echo "write ignore file list"
echo '/*'>'.git/info/sparse-checkout'



echo '!/'$audio_path>>'.git/info/sparse-checkout'
echo '!/'$audio_path'.meta'>>'.git/info/sparse-checkout'

echo '!/'$clothe_icon_path>>'.git/info/sparse-checkout'
echo '!/'$clothe_icon_path'.meta'>>'.git/info/sparse-checkout'

echo '!/'$clothe_spine_path>>'.git/info/sparse-checkout'
echo '!/'$clothe_spine_path'.meta'>>'.git/info/sparse-checkout'

echo '!/'$clothes_spine_pre_path>>'.git/info/sparse-checkout'
echo '!/'$clothes_spine_pre_path'.meta'>>'.git/info/sparse-checkout'

echo '/'$clothe_icon_path'/base'>>'.git/info/sparse-checkout'
echo '/'$clothe_icon_path'/base.meta'>>'.git/info/sparse-checkout'

echo '/'$baremold'.atlas'>>'.git/info/sparse-checkout'
echo '/'$baremold'.atlas.meta'>>'.git/info/sparse-checkout'

echo '/'$baremold'.json'>>'.git/info/sparse-checkout'
echo '/'$baremold'.json.meta'>>'.git/info/sparse-checkout'

echo '/'$baremold'.png'>>'.git/info/sparse-checkout'
echo '/'$baremold'.png.meta'>>'.git/info/sparse-checkout'

echo '/'$black_baremold'.atlas'>>'.git/info/sparse-checkout'
echo '/'$black_baremold'.atlas.meta'>>'.git/info/sparse-checkout'

echo '/'$black_baremold'.json'>>'.git/info/sparse-checkout'
echo '/'$black_baremold'.json.meta'>>'.git/info/sparse-checkout'

echo '/'$black_baremold'.png'>>'.git/info/sparse-checkout'
echo '/'$black_baremold'.png.meta'>>'.git/info/sparse-checkout'

echo "done"

echo "apply git settings"
git sparse-checkout reapply
echo "all done!"

read -n 1

```