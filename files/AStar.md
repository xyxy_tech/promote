## A* 工具类
```typescript
import BaseGrid from "./BaseGrid";

export default class AStar {

    static findPath(from: BaseGrid, to: BaseGrid): BaseGrid[] {
        let openGrids: BaseGrid[] = [];
        let closeGrids: BaseGrid[] = [];

        let currentGrid = from;

        currentGrid.g = 0;
        currentGrid.h = currentGrid.getCostWithGrid(to);

        openGrids.push(currentGrid);

        while (openGrids.length) {
            openGrids.sort((g1, g2) => {
                return g1.f - g2.f;
            });
            currentGrid = openGrids.shift();
            closeGrids.push(currentGrid);

            if (closeGrids.indexOf(to) != -1) {
                break;
            }

            let g = currentGrid.g + 1;
            if (!currentGrid.neighbors.length) {
                currentGrid.findNeighbors();
            }
            currentGrid.neighbors.forEach(nGrid => {

                if (nGrid.walkable == false){
                    return;
                }
                if (closeGrids.indexOf(nGrid) != -1) {
                    return;
                }

                if (openGrids.indexOf(nGrid) == -1) {
                    nGrid.g = g;
                    nGrid.h = nGrid.getCostWithGrid(to)
                    openGrids.push(nGrid);
                } else {
                    nGrid.g = Math.min(nGrid.g, g);
                }

            })
        }

        let path: BaseGrid[] = [];
        //找出路径
        if (closeGrids.indexOf(to) != -1) {
            currentGrid = to;
            path.push(currentGrid);
            for (let i = to.g - 1; i >= 0; i--) {
                currentGrid = closeGrids.filter(g => {
                    return g.g == i && g.neighbors.indexOf(currentGrid) != -1;
                })[0]
                path.push(currentGrid)
            }

        }
        return path.reverse();
    }

}
```
## 格子基类

```typescript
/**
 * 配合A*使用的格子基类 各种变体只需继承此类并实现基础方法即可
 * 
 */
export default class BaseGrid {
    //总消耗
    private _f: number;
    public get f(): number {
        return this._f;
    }
    //步数
    public g: number;
    //当前grid到终点步数
    public h: number;
    //坐标 需根据实际情况确定
    coor: number[];
    //是否可通过
    walkable: boolean;
    //邻居节点
    neighbors: BaseGrid[];
    //地图数据
    mapData: any;

    constructor(coor: number[], walkable: boolean = true, mapData: any) {
        this.coor = coor;
        this.walkable = walkable;
        this.mapData = mapData;
        this.neighbors = [];
    }
    //查找可行走的邻居节点
    findNeighbors() {
        throw "请复写该方法，实现查找邻居节点"
    }
    //计算两格子距离
    getCostWithGrid(target: BaseGrid): number {
        throw "请复写该方法，实现计算消耗"
    }
    //使用坐标获取格子
    getGridWithCoor(coor: number[]): BaseGrid {
        throw "请复写该方法，实现通过坐标获取grid"
    }
    //重置
    reset() {
        // this.neighbors = [];//邻居不用滞空
        this.g = this.h = 0;
    }

}


```

## 六边形寻路实现
```typescript
import AStar from "./AStar/AStar";
import HexagonGrid from "./HexagonGrid";

export default class HexagonAstar {

    _gridSize: cc.Size;

    //二维坐标和六边形坐标映射
    _phyMapData: HexagonGrid[][] = [[]];
    //格子六边形左边索引 方便查找
    _mapData: { [index: string]: HexagonGrid } = {};

    /**
     * 
     * @param w 地图宽度
     * @param h 地图高度
     * @param gridSize  格子大小
     */
    initMap(w, h, gridSize) {
        cc.log('initMap', w, h, gridSize)
        this._gridSize = gridSize;
        let xOffset = 0;
        for (let y = 0; y < h; y++) {
            if (y > 0 && y % 2 == 0) {
                xOffset--;
            }
            for (let x = xOffset; x < w + xOffset; x++) {
                let walkable = false

                let z = -(x + y);
                let grid = new HexagonGrid([x, y, z], walkable, this._mapData);

                if (!this._phyMapData[y]) {
                    this._phyMapData[y] = [];
                }
                this._mapData[`${x}_${y}_${z}`] = grid
                let corx = this._phyMapData[y].push(grid) - 1;
                grid.coor2d = cc.v2(corx, y);
            

            }
        }
    }

    setWalkable(grid: HexagonGrid, walkable) {

    }

    getGrid(coor2d){
        let x = coor2d[0];
        let y = coor2d[1];
        if (!this._phyMapData[y]) {
            return null;
        }

        return this._phyMapData[y][x];
    }

    findPath(from: HexagonGrid, to: HexagonGrid) {
        return AStar.findPath(from, to);
    }


    //坐标转位置
    //当前设定 initMap传入的gridSize 宽高为格子图片分辨率，也就是宽度是六边形的肩宽 高度是六边形边长的2倍 
    coorToLoc(coor: number[]): cc.Vec2 {
        let x = coor[0] * this._gridSize.width + coor[1] * this._gridSize.width / 2; // 每高一层 x要再偏移0.5肩宽
        let y = coor[1] * this._gridSize.height * (3 / 4); // y间隔 3/4格子高度
        return cc.v2(x, y)
    }

    coorToLoc2D(coor2d: number[]): cc.Vec2 {
        let x = coor2d[0] * this._gridSize.width + (coor2d[1]%2 == 1 ? this._gridSize.width / 2 : 0); // 奇数层x要再偏移0.5肩宽
        let y = coor2d[1] * this._gridSize.height * (3 / 4); // y间隔 3/4格子高度
        return cc.v2(x, y)
    }
    //位置转坐标
    locToCoor(loc: cc.Vec2) {

    }

    dump() {
        console.log(this)
    }

}


```

## 六边形格子
```typescript
import BaseGrid from "./AStar/BaseGrid";

//六边形格子
//宽高比大概是1.732/2 = 0.866
export default class HexagonGrid extends BaseGrid {

    coor2d:cc.Vec2;
    data:any;

    //mapData全部初始化完成后可用
    public findNeighbors() {
        //六角形坐标守恒定律！ x+y+z = 0
        //top-left
        let tlNb = this.getGridWithCoor([this.coor[0] - 1, this.coor[1] + 1, this.coor[2]]);
        if (tlNb) {
            this.neighbors.push(tlNb);
        }
        //top-right
        let trNb = this.getGridWithCoor([this.coor[0], this.coor[1] + 1, this.coor[2] - 1]);
        if (trNb) {
            this.neighbors.push(trNb);
        }
        //left
        let lNb = this.getGridWithCoor([this.coor[0] - 1, this.coor[1], this.coor[2] + 1]);
        if (lNb) {
            this.neighbors.push(lNb);
        }
        //right
        let rNb = this.getGridWithCoor([this.coor[0] + 1, this.coor[1], this.coor[2] - 1]);
        if (rNb) {
            this.neighbors.push(rNb);
        }
        //bottom-left
        let blNb = this.getGridWithCoor([this.coor[0], this.coor[1] - 1, this.coor[2] + 1]);
        if (blNb) {
            this.neighbors.push(blNb);
        }
        //bottom-right
        let brNb = this.getGridWithCoor([this.coor[0] + 1, this.coor[1] - 1, this.coor[2]]);
        if (brNb) {
            this.neighbors.push(brNb);
        }
        return this.neighbors;
    }

    public getCostWithGrid(targetGrid: HexagonGrid) {
        return Math.max(Math.abs(this.coor[0] - targetGrid.coor[0]), Math.abs(this.coor[1] - targetGrid.coor[1]), Math.abs(this.coor[2] - targetGrid.coor[2]))
    }

    public getGridWithCoor(coor: number[]): HexagonGrid {
        return this.mapData[this.generateIdWithCoor(coor)]
    }

    public generateIdWithCoor(coor) {
        return `${coor[0]}_${coor[1]}_${coor[2]}`;
    };

    public generateId() {
        return this.generateIdWithCoor(this.coor);
    }


}

```
